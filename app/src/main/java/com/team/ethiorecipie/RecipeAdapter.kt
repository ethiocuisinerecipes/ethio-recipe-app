package com.team.ethiorecipie
/*
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.team.ethiorecipie.data.Recipe
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class RecipeAdapter(val context: Context): RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {
    private val recipes = listOf(
        Recipe(
            "Doro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken, Cook the chicken, " +
                    "Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken," +
                    "Cook the chicken,Cook the chicken,Cook the chicken,Cook the chicken,Cook the chicken" +
                    " Cook the chicken,Cook the chicken,Cook the chicken,Cook the chickenCook the chicken"
        ),
        Recipe(
            "Shiro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Beg Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Alecha Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Kekel Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Tibs",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Qwuanta Firfir",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Dirkosh Firfir",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Beyaynetu",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Qurt",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        )

    )



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = inflater.inflate(R.layout.recycler_view_item,parent,false)


        recyclerViewItem.setOnClickListener {
            val recipeDetailIntent = Intent(context, RecipeDetailActivity::class.java)

            val recipeTitle = it.text1.text

            val recipe = recipes.find { it.title == recipeTitle }

            if (recipe != null){
                recipeDetailIntent.putExtra("Title",recipe.title)
                recipeDetailIntent.putExtra("Summery",recipe.summery)
                recipeDetailIntent.putExtra("Ingredients",recipe.ingredients)
                recipeDetailIntent.putExtra("Preparation",recipe.preparation)
            }
            context.startActivity(recipeDetailIntent)
        }

        return RecipeViewHolder(recyclerViewItem)

    }

    override fun getItemCount(): Int {
        return recipes.size
    }

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val recipe = recipes[position]


        holder.itemView.text1.text = recipe.title
        holder.itemView.text2.text = recipe.summery

    }

    class RecipeViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
}*/


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.team.ethiorecipie.data.Recipe
import kotlinx.android.synthetic.main.recycler_view_item.view.*

class RecipeAdapter(val context: Context):
    RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private var recipes: List<Recipe> = listOf(
        Recipe(
            "Doro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken, Cook the chicken, " +
                    "Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken, Cook the chicken," +
                    "Cook the chicken,Cook the chicken,Cook the chicken,Cook the chicken,Cook the chicken" +
                    " Cook the chicken,Cook the chicken,Cook the chicken,Cook the chickenCook the chicken"
        ),
        Recipe(
            "Shiro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Beg Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Alecha Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Kekel Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Tibs",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Qwuanta Firfir",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Dirkosh Firfir",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Beyaynetu",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Qurt",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        )

    )


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {

        val recyclerViewItem =
            inflater
                .inflate(R.layout.recycler_view_item, parent, false)

        recyclerViewItem.setOnClickListener {
            val recipeDetailIntent = Intent(context,RecipeDetailActivity::class.java)

            val recipeTitle = it.text1.text

            val recipe = recipes.find { it.title == recipeTitle }

            if (recipe != null){
                recipeDetailIntent.putExtra("Title",recipe.title)
                recipeDetailIntent.putExtra("Summery",recipe.summery)
                recipeDetailIntent.putExtra("Ingredients",recipe.ingredients)
                recipeDetailIntent.putExtra("Preparation",recipe.preparation)
            }
            context.startActivity(recipeDetailIntent)
        }


        return RecipeViewHolder(recyclerViewItem)
    }

    override fun getItemCount() = recipes.size

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val recipe = recipes[position]
        holder.codeTextView.text = recipe.title
        holder.titleTextView.text = recipe.summery
    }

    internal fun setRecipes(recipes: List<Recipe>){
        this.recipes = recipes
        notifyDataSetChanged()
    }

    inner class RecipeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val codeTextView = itemView.text1
        val titleTextView = itemView.text2
    }
}


/*
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.team.ethiorecipie.data.Recipe
import kotlinx.android.synthetic.main.recycler_view_item.view.*
import java.util.*

class RecipeAdapter(context: Context):
    RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private var recipes: List<Recipe> = listOf(
        Recipe(
            "Doro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Kwanta",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Kikil",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Tibis",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Yoo hooo",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Beg Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Firfir Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        ),
        Recipe(
            "Shiro Wote",
            "Doro Wote is Ethiopian Traditional Food",
            "One full grown Chicken",
            "Cook the chicken"
        )

    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeViewHolder {

        //val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = inflater.inflate(R.layout.recycler_view_item, parent, false)
        return RecipeViewHolder(recyclerViewItem)
    }
        /*recyclerViewItem.setOnClickListener {
            val recipeDetailIntent = Intent(context,RecipeDetailActivity::class.java)

            val recipeTitle = it.text1.text

            val recipe = recipes.find { it.title == recipeTitle }

            if (recipe != null){
                recipeDetailIntent.putExtra("Title",recipe.title)
                recipeDetailIntent.putExtra("Summery",recipe.summery)
                recipeDetailIntent.putExtra("Ingredients",recipe.ingredients)
                recipeDetailIntent.putExtra("Preparation",recipe.preparation)
            }
            context.startActivity(recipeDetailIntent)
        }

        return RecipeViewHolder(recyclerViewItem)

    }*/

    override fun getItemCount() = recipes.size

    override fun onBindViewHolder(holder: RecipeViewHolder, position: Int) {
        val recipe = recipes[position]
        holder.codeTextView.text = recipe.title
        holder.titleTextView.text = recipe.summery

    }
    internal fun setRecipes(recipes: List<Recipe>){
        this.recipes = recipes
        notifyDataSetChanged()
    }

    inner class RecipeViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    {
        val codeTextView = itemView.text1
        val titleTextView = itemView.text2
    }
}
*/

