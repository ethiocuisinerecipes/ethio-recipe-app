package com.team.ethiorecipie.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity (tableName = "recipes")
data class Recipe(
    @PrimaryKey @ColumnInfo (name = "recipe_title") val title:String,
    @ColumnInfo (name = "recipe_summery") val summery:String,
    @ColumnInfo (name = "recipe_ingredients") val ingredients:String,
    @ColumnInfo (name = "recipe_preparation") val preparation:String
):Serializable
