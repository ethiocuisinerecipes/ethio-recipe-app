package com.team.ethiorecipie.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RecipeDao {

    @Query("SELECT * from recipes ORDER BY recipe_title")
    fun getAllRecipes(): LiveData<List<Recipe>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecipe(recipe: Recipe):Long

    /*
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateRecipe(recipe: Recipe):Long

    @Delete
    fun deleteRecipe(recipe: Recipe):Long
    */


}

