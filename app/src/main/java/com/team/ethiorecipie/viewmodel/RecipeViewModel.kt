package com.team.ethiorecipie.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.team.ethiorecipie.data.Recipe
import com.team.ethiorecipie.data.RecipeDatabase
import com.team.ethiorecipie.repository.RecipeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RecipeViewModel(application: Application): AndroidViewModel(application) {

    private val recipeRepository: RecipeRepository
    private val _likes =  MutableLiveData(0)
    val allRecipes: LiveData<List<Recipe>>
    val likes: LiveData<Int> = _likes

    init {
        val recipeDao = RecipeDatabase.getDatabase(application).recipeDao()
        recipeRepository = RecipeRepository(recipeDao)
        allRecipes = recipeRepository.allRecipes()
    }

    fun insertRecipe(recipe: Recipe) = viewModelScope.launch(Dispatchers.IO) {
        recipeRepository.insertRecipe(recipe)
    }

    val popularity: LiveData<Popularity> = Transformations.map(_likes) {
        when {
            it > 9 -> Popularity.STAR
            it > 4 -> Popularity.POPULAR
            else -> Popularity.NORMAL
        }
    }

    fun onLike() {
        _likes.value = (_likes.value ?: 0) + 1
    }
    /*
    fun updateRecipe(recipe: Recipe) {
        recipeRepository.updateRecipe(recipe)
    }

    fun deleteRecipe(recipe: Recipe){
        recipeRepository.deleteRecipe(recipe)
    }
    */

}


