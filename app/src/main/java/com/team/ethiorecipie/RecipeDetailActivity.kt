package com.team.ethiorecipie

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RatingBar
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.team.ethiorecipie.viewmodel.RecipeViewModel
import kotlinx.android.synthetic.main.activity_recipe_detail.*


const val SHARED_PREFERENCE_ID = "shared_preference_file_id"

class RecipeDetailActivity : AppCompatActivity(), RatingBar.OnRatingBarChangeListener {
    override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {
        with(sharedPref.edit()){
            putString(intent.getStringExtra("Title"), rating.toString())
            commit()
        }
        textView.text = "$rating"
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(RecipeViewModel::class.java)
    }


    lateinit var sharedPref: SharedPreferences

    lateinit var textView: TextView
    lateinit var ratingBar: RatingBar

    private fun loadText(){
        val savedText = sharedPref.getString(intent.getStringExtra("Title"),"Hello")
        textView.text = savedText
        //rating_bar.rating = savedText.toFloat()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: RecipeDetailBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_recipe_detail)

        binding.lifecycleOwner = this  // use Fragment.viewLifecycleOwner for fragments

        binding.viewmodel = viewModel


        setContentView(R.layout.activity_recipe_detail)

        detail_title_tv.text = intent.getStringExtra("Title")
        detail_summery_tv.text = intent.getStringExtra("Summery")
        detail_ingredients_tv.text = intent.getStringExtra("Ingredients")
        detail_preparation_tv.text = intent.getStringExtra("Preparation")


        sharedPref = getSharedPreferences(SHARED_PREFERENCE_ID, Context.MODE_PRIVATE)

        textView = rating_num
        //ratingBar = rating_bar

        ratingBar.onRatingBarChangeListener = this

        if (sharedPref.contains(intent.getStringExtra("Title"))){
            loadText()
        }


    }
}