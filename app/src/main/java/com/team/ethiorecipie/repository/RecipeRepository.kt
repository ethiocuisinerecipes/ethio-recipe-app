package com.team.ethiorecipie.repository

import androidx.lifecycle.LiveData
import com.team.ethiorecipie.data.Recipe
import com.team.ethiorecipie.data.RecipeDao

class RecipeRepository(private val recipeDao: RecipeDao) {

    fun allRecipes(): LiveData<List<Recipe>> = recipeDao.getAllRecipes()

    fun insertRecipe(recipe: Recipe) {
        recipeDao.insertRecipe(recipe)
    }

    /*
    fun deleteRecipe(recipe: Recipe) {
        recipeDao.deleteRecipe(recipe)

    }
    fun updateRecipe(recipe: Recipe) {
        recipeDao.updateRecipe(recipe)

    }
    */
}


