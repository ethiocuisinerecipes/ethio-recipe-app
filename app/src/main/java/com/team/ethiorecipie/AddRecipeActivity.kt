package com.team.ethiorecipie

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.team.ethiorecipie.data.Recipe
import kotlinx.android.synthetic.main.activity_add_recipe.*

class AddRecipeActivity : AppCompatActivity() {

    private lateinit var addButton: Button

    private lateinit var titleEditText: EditText
    private lateinit var summeryEditText: EditText
    private lateinit var ingredientsEditText: EditText
    private lateinit var preparationEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_recipe)

        addButton = add_button
        titleEditText = summery_edit_text
        summeryEditText = summery_edit_text
        ingredientsEditText = ingredients_edit_text
        preparationEditText = preparation_edit_text

        addButton.setOnClickListener {
            val recipe = readFields()
            val replyCourseIntent = Intent()
            replyCourseIntent.putExtra("RECIPE", recipe)
            setResult(Activity.RESULT_OK, replyCourseIntent)
            finish()
        }
    }

    private fun readFields() = Recipe(
        titleEditText.text.toString(),
        summeryEditText.text.toString(),
        ingredientsEditText.text.toString(),
        preparationEditText.text.toString()
    )
    private fun updateFields(recipe: Recipe){
        if (recipe!= null) {
            titleEditText.setText(recipe.title)
            summeryEditText.setText(recipe.summery)
            ingredientsEditText.setText(recipe.ingredients)
            preparationEditText.setText(recipe.preparation)
        }
    }
    private fun clearFields(recipe: Recipe){
            titleEditText.setText("")
            summeryEditText.setText("")
            ingredientsEditText.setText("")
            preparationEditText.setText("")

    }

}
